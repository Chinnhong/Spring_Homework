package com.example.demo.controller;

import com.example.demo.model.Category;
import com.example.demo.service.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    @GetMapping("/addC")
    public String addCategory(Model model){
        model.addAttribute("categories",new Category());
        return "addcategory";
    }
    @PostMapping("/addC")
    public String saveCategory(@Valid @ModelAttribute Category category){
        categoryService.insert(category);
        return "redirect:/categories";
    }
    @GetMapping("/categories")
    public String allCategories(Model model){
        model.addAttribute("categories",categoryService.findAll());
        return "/categories";
    }
    @GetMapping("/editcategory/{id}")
    public String edit(@PathVariable("id") int id, Model model){
        model.addAttribute("category",categoryService.findOne(id));
        return "editcategory";
    }
    @PostMapping("/editcategory")
    public String saveEdit( @Valid @ModelAttribute Category category, Model model){
        categoryService.edit(category);
        return "redirect:/categories";
    }
    @GetMapping("/deletecategory/{id}")
    public String deletecategory(@PathVariable int id){
        categoryService.delete(id);
        return "redirect:/categories";
    }
}
