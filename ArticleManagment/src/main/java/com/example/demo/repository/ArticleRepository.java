package com.example.demo.repository;

import com.example.demo.model.Article;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;


import java.util.List;
@Repository
public interface ArticleRepository {
    @Insert("INSERT INTO tb_articles (title,description, author,thumbnail,created_date,category_id) VALUES (#{title},#{description},#{author},#{thumbnail},#{createDate},#{category.id})")
    public void insert(Article article);
    @Select("SELECT a.id,a.title,a.description, a.author,a.thumbnail,a.created_date," +
            "a.category_id,c.name as category_name from tb_articles a INNER JOIN tb_categories c " +
            "ON a.category_id = c.id WHERE a.id=#{id}")
    @Results({
            @Result(property="createDate",column="created_date"),
            @Result(property ="category.id",column = "category_id"),
            @Result(property ="category.category",column = "category_name")
    })
    public Article findOne(int id);
    @Select("SELECT a.id,a.title,a.description, a.author,a.thumbnail,a.created_date," +
            "a.category_id,c.name as category_name from tb_articles a INNER JOIN tb_categories c " +
            "ON a.category_id = c.id ORDER by id")
    @Results({
            @Result(property="createDate",column="created_date"),
            @Result(property ="category.id",column = "category_id"),
            @Result(property ="category.category",column = "category_name")
    })
    public List<Article> findAll();
    @Delete("DELETE FROM tb_articles WHERE id=#{id}")
    public void delete(int id);
    @Update("UPDATE tb_articles SET title=#{title},description=#{description},author=#{author},category_id=#{category.id},thumbnail=#{thumbnail} WHERE id=#{id}")
    public void edit(Article article);


}
