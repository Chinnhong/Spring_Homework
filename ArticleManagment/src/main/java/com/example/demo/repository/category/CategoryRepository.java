package com.example.demo.repository.category;

import com.example.demo.model.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public interface CategoryRepository {


    @Select("SELECT * FROM tb_categories")
    @Results({
    @Result(property = "category",column = "name")
    })
    public List<Category> findAll();
    @Select("SELECT * FROM tb_categories WHERE id=#{id}")
    @Results({
            @Result(property = "category",column = "name")
    })
    public Category findOne(int id);
    @Select("insert into tb_categories (name) values(#{category})")
    public void insert(Category c);
    @Update("UPDATE tb_categories set name=#{category} where id=#{id}")
    public void edit(Category category);
    @Delete("Delete tb_categories where id=#{id}")
    void delte(int id);

}
