CREATE TABLE tb_categories(
  id INT PRIMARY KEY auto_increment,
  name varchar not NULL
);
CREATE TABLE tb_articles(
  id int PRIMARY KEY auto_increment,
  title VARCHAR not NULL ,
  description VARCHAR NOT  NULL ,
  thumbnail VARCHAR NOT  NULL ,
  author VARCHAR NOT  NULL ,
  created_date VARCHAR NOT NULL ,
  category_id INT REFERENCES tb_categories on DELETE CASCADE on UPDATE CASCADE
);